package insurance;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Tataaia {
    public static void main(String[] args) throws InterruptedException, IOException {
        System.setProperty("webdriver.chrome.driver","src/test/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        JavascriptExecutor js=(JavascriptExecutor) driver;
        driver.manage().window().maximize();
        driver.get("https://apps.tataaia.com/ULNAV/funds/daily-nav.jsp");
        Thread.sleep(5000);
        js.executeScript("scrollBy(0, 2500)");
        Thread.sleep(500);
        //fund selection
        List<WebElement>list1=driver.findElements(By.xpath("//*[@id=\"ddlFund\"]"));
        for (int a1=0; a1<list1.size();a1++){

            if (list1.get(a1).getText().contains("Stable Growth(ULIF 007 01/07/06 TSL 110)")){

                list1.get(a1).click();

            }
        }
        //day
        Thread.sleep(400);
        List<WebElement>list34=driver.findElements(By.xpath("//*[@id=\"ddlTimePerioddd\"]"));
        for (int a2=0;a2<list34.size();a2++){

            if (list34.get(a2).getText().contains("6")){

                list34.get(a2).click();
            }
        }

        Thread.sleep(400);
        //month
        List<WebElement>list67=driver.findElements(By.xpath("//*[@id=\"ddlTimePeriodmm\"]"));
        for (int a3=0; a3<list67.size();a3++){

            if (list67.get(a3).getText().contains("May")){

                list67.get(a3).click();
            }
        }
        Thread.sleep(400);
        //year
        List<WebElement>list70=driver.findElements(By.xpath("//*[@id=\"ddlTimePeriodyy\"]"));
        for (int a4=0;a4<list70.size();a4++){

            if (list70.get(a4).getText().contains("2018")){

                list70.get(a4).click();
            }
        }
        Thread.sleep(400);
        WebElement elmt1=driver.findElement(By.xpath("//*[@id=\"frmArchivedNAVs\"]/input"));
        elmt1.click();
        //information collection
        String st5=driver.getTitle();
        String st8=driver.findElement(By.xpath("//*[@id=\"prodOver\"]/div/div[2]/p")).getText();
        StringBuilder stb=new StringBuilder();
        stb.append(st5);
        stb.append(",");
        stb.append(st8);
        //csv file creation
        FileWriter fw1=new FileWriter("src/test/resources/tataaia.csv");
        fw1.write(stb.toString());
        fw1.close();
    }
}
