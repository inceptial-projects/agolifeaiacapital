package insurance;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.io.FileWriter;
import java.io.IOException;
import java.time.Duration;
import java.util.List;

public class Egonlife{
    public static void main(String[] args) throws InterruptedException, IOException {
        System.setProperty("webdriver.chrome.driver","src/test/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        driver.manage().window().maximize();
        driver.get("https://www.aegonlife.com/fund-performance/nav-rate?field_scheme_tid=3&field_published_date_value%5Bmin%5D%5Bdate%5D=29-09-2021&field_published_date_value%5Bmax%5D%5Bdate%5D=29-09-2021");
        Thread.sleep(2000);
        //fund selection
//        List<WebElement>list87=driver.findElements(By.xpath("//*[@id=\"edit-field-scheme-tid\"]"));
//
//        for (int b1=0;b1<list87.size();b1++){
//
//            if (list87.get(b1).getText().contains("CONSERVATIVE FUND")){
//                Thread.sleep(6000);
//                list87.get(b1).click();
//            }
//        }
         WebElement element= driver.findElement(By.xpath("//*[@id=\"edit-field-scheme-tid\"]"));
        Select drpdn=new Select(element);
        drpdn.selectByValue("5");


        //Start  date
        driver.findElement(By.xpath("//*[@id=\"edit-field-published-date-value-min-datepicker-popup-0\"]")).click();
        new WebDriverWait(driver, Duration.ofSeconds(5)).until(ExpectedConditions.visibilityOfElementLocated(By.name("field_published_date_value[min][date]")));
//        //month
//        List<WebElement>list1=driver.findElements(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]"));
//        Thread.sleep(6000);
//        for (int a1=0;a1<list1.size();a1++){
//
//            if (list1.get(a1).getText().contains("Feb")){
//                Thread.sleep(6000);
//                list1.get(a1).click();
//            }
//        }
            WebElement element1=driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]"));
            Select drpn1=new Select(element1);
            drpn1.selectByValue("4");
        //year
//        List<WebElement>lst2=driver.findElements(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]"));
//
//        for (int b1=0;b1<lst2.size();b1++){
//
//            if (lst2.get(b1).getText().contains("2014")){
//                Thread.sleep(6000);
//                lst2.get(b1).click();
//            }
//        }
        WebElement element2=driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]"));
        Select drpn2=new Select(element2);
        drpn2.selectByValue("2016");
        Thread.sleep(5000);
        driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[4]/a")).click();

        //End date
        driver.findElement(By.xpath("//*[@id=\"edit-field-published-date-value-max-datepicker-popup-0\"]")).click();
        new WebDriverWait(driver,Duration.ofSeconds(5)).until(ExpectedConditions.visibilityOfElementLocated(By.xpath("//*[@id=\"edit-field-published-date-value-max-datepicker-popup-0\"]")));
       //month
//        List<WebElement>lst4=driver.findElements(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[1]"));
//        for (int c1=0;c1<lst4.size();c1++){
//
//            if (lst4.get(c1).getText().contains("Apr")){
//
//                lst4.get(c1).click();
//            }
//        }
        WebElement element3=driver.findElement(By.xpath("/html/body/div[6]/div/div/select[1]"));
        Select drp4=new Select(element3);
        drp4.selectByValue("5");
        //year
//        List<WebElement>lst3=driver.findElements(By.xpath("//*[@id=\"ui-datepicker-div\"]/div/div/select[2]"));
//        for (int d1=0;d1<lst3.size();d1++){
//
//            if (lst3.get(d1).getText().contains("2018")){
//
//                lst3.get(d1).click();
//            }
//        }
            WebElement element4=driver.findElement(By.xpath("/html/body/div[6]/div/div/select[2]"));
            Select drpn6=new Select(element4);
            drp4.selectByValue("2017");
        Thread.sleep(2000);
        //day
        driver.findElement(By.xpath("//*[@id=\"ui-datepicker-div\"]/table/tbody/tr[3]/td[5]/a")).click();
//
//        //search
        driver.findElement(By.xpath("//*[@id=\"edit-submit-nav-rates\"]")).click();
//        //list of info
        String st1=driver.getTitle();
        String st2=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[1]/td[1]/span")).getText();
        String str3=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[1]/td[2]")).getText();
        String str4=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[1]/td[4]")).getText();
        String str5=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[2]/td[1]/span")).getText();
        String str6=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[2]/td[2]")).getText();
        String str7=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[2]/td[4]")).getText();
        String str8=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[3]/td[1]/span")).getText();
        String str9=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[3]/td[2]")).getText();
        String str10=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[3]/td[4]")).getText();
        String str11=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[4]/td[1]/span")).getText();
        String str12=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[4]/td[2]")).getText();
        String str13=driver.findElement(By.xpath("//*[@id=\"content\"]/div[1]/div[2]/table/tbody/tr[4]/td[4]")).getText();
        StringBuilder stb1=new StringBuilder();
        stb1.append(st1);
        stb1.append(",");
        stb1.append(st2);
        stb1.append(",");
        stb1.append(str3);
        stb1.append("\n");
        stb1.append(str4);
        stb1.append(",");
        stb1.append(str5);
        stb1.append(",");
        stb1.append(str6);
        stb1.append("\n");
        stb1.append(str7);
        stb1.append(",");
        stb1.append(str8);
        stb1.append(",");
        stb1.append(str9);
        stb1.append("\n");
        stb1.append(str10);
        stb1.append(",");
        stb1.append(str11);
        stb1.append(",");
        stb1.append(str12);
        stb1.append(",");
        stb1.append(str13);

        //csv file creation
        FileWriter wrt1=new FileWriter("src/test/resources/allinsurances.csv");
        wrt1.write(stb1.toString());
        wrt1.close();


    }
}
