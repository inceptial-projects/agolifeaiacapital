package insurance;

import com.opencsv.CSVWriter;
import net.bytebuddy.implementation.bytecode.Division;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class BirlaCapital {
    public static void main(String[] args) throws InterruptedException, IOException {
        System.setProperty("webdriver.chrome.driver","src/test/chromedriver_win32/chromedriver.exe");
        WebDriver driver=new ChromeDriver();
        JavascriptExecutor js=(JavascriptExecutor)driver;
        driver.manage().window().maximize();
        driver.get("https://lifeinsurance.adityabirlacapital.com/about-us/know-our-funds?fund=E8QC9ahGVgtSodoEE5AuoaEnewdyP6WSIzsA82lD3L1dA2UTtPBhrEXlR0f1scRE");
        Thread.sleep(5000);
        js.executeScript("scrollBy(0, 4500)");
        WebElement element8=driver.findElement(By.className("selectize-input"));
        element8.click();
        Thread.sleep(400);
        List<WebElement> element10=driver.findElements(By.xpath("//*[@id=\"download-brochure-form\"]/div/div/div[2]/div"));
        for (int i=0; i<element10.size();i++){

            if (element10.get(i).getText().contains("Balancer Fund")){

                element10.get(i).click();
            }
        }
        Thread.sleep(5000);
        String st1=driver.getTitle();
        String st2=driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[7]/ul/li[4]/p")).getText();
        String st3=driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[7]/ul/li[3]/p")).getText();
        String st4=driver.findElement(By.xpath("/html/body/div[3]/div[1]/div/div/div[7]/ul/li[2]/p")).getText();
        //information collection
        StringBuilder sbd=new StringBuilder();
        sbd.append("Aditya Birla : \n");
        sbd.append(st1);
        sbd.append(",");
        sbd.append(st2);
        sbd.append(",");
        sbd.append(st3);
        sbd.append(",");
        sbd.append(st4);
        //csv file creation
       FileWriter pw=new FileWriter("src/test/resources/allinsurances.csv");
        pw.write(sbd.toString());
        pw.close();

    }
}
